<?php

namespace Drupal\panopoly_search\EventSubscriber;

use Drupal\panopoly_admin\Event\AdvancedBlockEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for marking the blocks from Panopoly Search as advanced.
 */
class PanopolySearchAdvancedBlockEventSubscriber implements EventSubscriberInterface {

  const ADVANCED_PREFIXES = [
    'facet_block:',
  ];

  const ADVANCED_BLOCKS = [
    'panopoly_search_box' => TRUE,
    'views_block:panopoly_search_db-block' => TRUE,
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    if (class_exists('Drupal\panopoly_admin\Event\AdvancedBlockEvent')) {
      $events[AdvancedBlockEvent::class][] = ['onAdvancedBlockEvent'];
    }
    return $events;
  }

  /**
   * Marks default blocks from Panopoly as advanced.
   *
   * @param \Drupal\panopoly_admin\Event\AdvancedBlockEvent $event
   *   The event.
   */
  public function onAdvancedBlockEvent(AdvancedBlockEvent $event) {
    $event->setAdvanced($event->getAdvanced() + static::ADVANCED_BLOCKS);

    foreach ($event->getDefinitions() as $block_id => $definition) {
      foreach (static::ADVANCED_PREFIXES as $prefix) {
        if (strpos($block_id, $prefix) === 0) {
          $event->markAsAdvanced($block_id);
        }
      }
    }
  }

}
