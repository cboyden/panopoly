Feature: Panopoly Magic allows the user to change the "Display Type" of a Views widget
  In order to build really flexible Views widgets
  As a site administrator
  I need to be able to change the "Display Type" of a Views widget

  @api @javascript @panopoly_magic @panopoly2
  Scenario: Add Views widget set to Fields with 'Display Type' override allowed
    Given I am logged in as a user with the "administrator" role
      And I am viewing a landing page
    When I click "Layout"
    And I click "Add block in Section 1, Content region"
    And I click "Lists (Views)"
    And I click "Add Magic Display Type: Fields and allowed" in the "Layout Builder dialog"
      And I should see "Display Type" in the "legend" element in the "Layout Builder dialog" region
    Then the "settings[override][display_settings][view_settings]" field should contain "fields"

  @api @javascript @panopoly_magic @panopoly2
  Scenario: Add Views widget set to Fields but WITHOUT the 'Display Type' override allowed
    Given I am logged in as a user with the "administrator" role
      And I am viewing a landing page
    When I click "Layout"
    And I click "Add block in Section 1, Content region"
    And I click "Lists (Views)"
    And I click "Add Magic Display Type: Fields but not allowed" in the "Layout Builder dialog"
      And I should not see "Display Type" in the "legend" element in the "Layout Builder dialog" region
