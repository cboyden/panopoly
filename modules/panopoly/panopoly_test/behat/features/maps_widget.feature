Feature: Add map widget
  In order to put a map on a page
  As a site administrator
  I need to be able to use the map widget

  @api @javascript @panopoly_widgets @panopoly2 @panopoly_widgets_map
  Scenario: Add map to a page
    Given I am logged in as a user with the "administrator" role
      And I am viewing a landing page
    When I click "Layout"
      And I click "Add block in Section 1, Content region"
    And I click "Add Map"
    When I fill in the following:
      | Title       | Widget title            |
      | Text format | restricted_html         |
      | Information | Testing text body field |
      | Address     | Ørnebjergvej 28, Vejle  |
      And I press "Save" in the "Dialog buttons" region
    And I press "Save layout"
    Then I should see "Widget title"
      And I should see "Testing text body field"
