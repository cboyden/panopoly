/**
 * @file
 * Defines the behavior of the panopoly_media_browser view.
 */

(function ($) {

  "use strict";

  /**
   * Update the class of a row based on the status of a checkbox.
   *
   * @param {object} $row
   * @param {object} $input
   */
  function updateClasses($row, $input) {
    $row[$input.prop('checked') ? 'addClass' : 'removeClass']('checked');
  }

  /**
   * Attaches the behavior of the media entity browser view.
   */
  Drupal.behaviors.panopolyMediaBrowser = {
    attach: function (context, settings) {
      // Run through each row to add the default classes.
      $('.views-row', context).each(function() {
        var $row = $(this);
        var $input = $row.find('.views-field-entity-browser-select input');
        updateClasses($row, $input);
      });

      // Add a checked class when clicked.
      function toggleItem() {
        var $row = $(this);
        var $input = $row.find('.views-field-entity-browser-select input');
        $input.prop('checked', !$input.prop('checked'));
        updateClasses($row, $input);
      }
      $(once('panopoly-media-browser', '.views-row', context))
        .click(toggleItem)
        .keydown(function (event) {
          if (event.keyCode === 32 || event.keyCode == 13) {
            toggleItem.call(this);
            return false;
          }
        });
    }
  };

}(jQuery, Drupal));
