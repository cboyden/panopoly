<?php

namespace Drupal\panopoly_magic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Panopoly Magic.
 */
class PanopolyMagicPreviewSettingsForm extends ConfigFormBase {

  /**
   * The name of the config object that holds our settings.
   *
   * @var string
   */
  const CONFIG = 'panopoly_magic.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::CONFIG];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'panopoly_magic_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG);

    $form['live_preview'] = [
      '#title' => $this->t('Live Preview'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => [
        'automatic' => $this->t('Automatic'),
        'manual' => $this->t('Manual'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $config->get('live_preview'),
      '#description' => $this->t('Do you want to show a live preview while editing blocks in Layout Builder?'),
    ];

    $form['choose_block_preview'] = [
      '#title' => $this->t('Preview while choosing a block'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => [
        'automatic' => $this->t('Automatic'),
        'manual' => $this->t('Manual'),
        'single' => $this->t('Single'),
        'disabled' => $this->t('Disabled'),
      ],
      '#default_value' => $config->get('choose_block_preview'),
      '#description' => $this->t('Do you want to show a preview of the available blocks when choosing a block in Layout Builder?'),
    ];

    $form['choose_block_descriptions'] = [
      '#title' => $this->t('Show block descriptions when choosing a block'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('choose_block_descriptions'),
      '#description' => $this->t('Do you want to show text descriptions for available blocks when choosing a block in Layout Builder?'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::CONFIG);
    $config->set('live_preview', $form_state->getValue('live_preview'));
    $config->set('choose_block_preview', $form_state->getValue('choose_block_preview'));
    $config->set('choose_block_descriptions', $form_state->getValue('choose_block_descriptions'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
